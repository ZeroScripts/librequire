local RunService = game:GetService("RunService")
local HttpService = game:GetService("HttpService")

if RunService:IsStudio() then
    warn("librequire doesn't work in Roblox Studio")
    return
end

if getgenv().ZeroScripts and getgenv().ZeroScripts.librequire then
    return getgenv().ZeroScripts.librequire
end

local rbxRequire = getgenv()["require"]

local librequire = {
    _repo_url = "https://zeroscripts.gitlab.io/libraries/",
    _cache = {},
    _cache_type = "aot",
    _metadata = nil
}
librequire.__index = librequire
librequire.__call = function(self, module, args)
    if args == nil then
        args = {}
    end
    if not self._metadata then
        self:downloadMetadata()
    end
    local package = self:findPackage(module)
    if not package then
        error("Package " .. module .. " not found")
        return
    end
    local url = self._repo_url .. self._metadata.base_dir .. package.url
    -- Load dependencies first
    for _, v in pairs(package.dependencies) do
        require(v)
    end
    if self._cache[url] then
        print("Cache detected for", url)
        return self:loadCache(url)
    end
    local success, result = pcall(loadstring, game:HttpGet(url))
    if success then
        local packageResult = result(unpack(args))
        self:createCache(url, result, packageResult, args)
        return packageResult
    else
        return rbxRequire(module)
    end
end

local require = {}
function require:setRepoUrl(url)
    self._repo_url = url
end
function require:downloadMetadata()
    local response = game:HttpGet(self._repo_url .. "metadata.json")
    local json = HttpService:JSONDecode(response)
    self._metadata = json
end
function require:findPackage(name)
    if not self._metadata then
        self:downloadMetadata()
    end
    for _, package in pairs(self._metadata.packages) do
        if package.name == name then
            return package
        end
    end
end
function require:setCacheType(type)
    self._cache_type = type
end
function require:createCache(name, bytecode, result, args)
    print("Cache created for " .. name .. ": " .. self._cache_type)
    if self._cache_type == "aot" then
        self._cache[name] = {
            type = self._cache_type,
            result = result
        }
    elseif self._cache_type == "bytecode" then
        self._cache[name] = {
            type = self._cache_type,
            load = bytecode,
            args = args
        } 
    end
end
function require:loadCache(cacheName)
    local cache = self._cache[cacheName]
    if not cache then
        error("Invalid cache ", cacheName)
        return
    end
    if cache.type == "bytecode" then
        return cache.load(unpack(cache.args))
    elseif cache.type == "aot" then
        return cache.result
    else
        error("Invalid cache type:", cache.type)
        return
    end
end
function require:replaceRbxRequire()
    getgenv()["require"] = self
end
setmetatable(require, librequire)
require("zsinit") -- Load necessary library
getgenv().ZeroScripts.librequire = require
return require